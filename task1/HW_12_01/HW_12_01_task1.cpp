﻿#include <conio.h>
#include <iostream>
#include <stdlib.h>
#include <time.h>
#include <Windows.h>
using namespace std;
const int Size = 6;
int data_arr[Size];

int input(string volume) {
	int input_number;
	cout << "Please input " << volume << ": ";
	cin >> input_number;
	cout << "\n";
	return input_number;
}
void data_ar(int mas[Size], int Size) {
	data_arr[0] = input("d1 ");
	data_arr[1] = input("m1 ");
	data_arr[2] = input("y1 ");
	data_arr[3] = input("d2 ");
	data_arr[4] = input("m2 ");
	data_arr[5] = input("y2 ");
}
int num_vis_year(int mas[Size], int Size, int num) { //num = 2 or 5

	int res = 0;
	for (int i = 0; i < data_arr[num]; i++) {
		if (i % 400 == 0) res++;
		else if ((i % 100 != 0) && (i % 4 == 0)) res++;
	}
	return res;
}
bool if_num_vis_year(int mas[Size], int Size, int num) { //num = 2 or 5

	bool res = false;
	for (int i = 0; i < data_arr[num]; i++) {
		if (i % 400 == 0) res = true;
		else if ((i % 100 != 0) && (i % 4 == 0)) res = true;
		else res = false;
	}
	return res;
}
int data(int mas[Size], int Size, int  num) { // num = 1 or 2
	int d, m, y, k = 0;
	int num_day_in_mon_NLY[12]{ 0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334 };
	int num_day_in_mon_LY[12]{ 0, 31, 60, 91, 121, 152, 182, 213, 244, 274, 305, 335 };
	switch (num)
	{
	case 1:
		d = data_arr[0];
		m = data_arr[1];
		y = data_arr[2];
		k += d;
		if (if_num_vis_year(data_arr, Size, 2) == false) k += num_day_in_mon_NLY[m - 1];
		else k += num_day_in_mon_LY[m - 1];
		k += (y - 1) * 365 + num_vis_year(data_arr, Size, 2);
		break;

	case 2:
		d = data_arr[3];
		m = data_arr[4];
		y = data_arr[5];
		k += d;
		if (if_num_vis_year(data_arr, Size, 5) == false) k += num_day_in_mon_NLY[m - 1];
		else k += num_day_in_mon_LY[m - 1];
		k += (y - 1) * 365 + num_vis_year(data_arr, Size, 5);
		break;
	default:
		break;
	}
	return k;
}
void dif_data(int mas[Size], int Size) {
	int res = 0;
	data_ar(data_arr, Size);
	res = data(data_arr, Size, 2) - data(data_arr, Size, 1);
	cout << "\n\nDifference between " << data_arr[0] << "." << data_arr[1] << "." << data_arr[2] << " and " << data_arr[3] << "." << data_arr[4] << "." << data_arr[5];
	cout << " is " << res << "\n\n";
}
//___________________________________________________________
//task2
/*Задание 2. Написать функцию, определяющую среднее
арифметическое элементов передаваемого ей массива.*/

template < typename T >
void init_array(T arr[], int Size, int start_value=0){
	srand(time(NULL));
	for (int i = 0; i < Size; i++){
		arr[i] = start_value+rand()%100;
		cout << arr[i] << "\t";
	}
	cout << endl;
}

template < typename T >
void averange_array(T arr[], int _size) {
	T averange = 0;
	for (int i = 0; i < _size; i++) {
		averange += arr[i];
	}
	printf("\n\naverange of array is %d\n\n",averange / _size);
}

//__________________________________________________________________
//Задание 3. Написать функцию, определяющую количество
//положительных, отрицательных и нулевых элементов
//передаваемого ей массива.

void counter_of_neg_zero_pos_value(int arr[], int _size) {
	int count_neg = 0, count_pos = 0, count_zero = 0;
	for (int i = 0; i < _size; i++) {
		if (arr[i] < 0) count_neg++;
		else if (arr[i] > 0) count_pos++;
		else count_zero++;
	}
	printf("\n\nNumber of negative value is %d\nNumber of positive value is %d\nNumber of zero is %d\n", count_neg, count_pos, count_zero);
}

int main()
{
	const int _size = 100;
	int array[_size];
	bool flafOfExit = false;
	int choose;
	do {
		printf("\n\n\tPlease input number of task. For exit please input 0     =>   ");
		cin >> choose;
		switch (choose)
		{
		case 1:
			dif_data(data_arr, Size);
			break;
		case 2:
			init_array(array, _size);
			averange_array(array, _size);
			break;
		case 3:
			init_array(array, _size,-50);
			counter_of_neg_zero_pos_value(array, _size);
			break;

		default:
			break;
		}
		if (choose == 0) {
			flafOfExit = true;
			return 0;
		}

	} while (flafOfExit == false);
	return 0;	
}
   

